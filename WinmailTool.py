#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import subprocess
import sys

class WinmailTool:
    def onMainWindowDestroy(self, object, data=None):
        Gtk.main_quit()

    def onOpenButtonClick(self, object, data=None):
        filename = self.fileChooser.get_filename()

        tempDir = str(subprocess.check_output(["mktemp", "-d"])).strip()
        subprocess.call(["tnef", "--file=" + filename, "--directory=" + tempDir])

        subprocess.call(["xdg-open", tempDir])
        sys.exit(0)

    def __init__(self):
        self.gladefile = "WinmailTool.glade"
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)

        self.builder.connect_signals(self)

        self.mainWindow = self.builder.get_object("mainWindow")
        self.fileChooser = self.builder.get_object("fileChooser")
        self.mainWindow.show()

if __name__ == "__main__":
    main = WinmailTool()
    Gtk.main()
